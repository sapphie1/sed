use core::f32;
use std::cell::RefCell;
use std::collections::HashMap;
use std::num::NonZeroU32;
use std::sync::Arc;

use async_winit::dpi::PhysicalSize;
use async_winit::event::{ElementState, VirtualKeyCode};
use async_winit::event_loop::EventLoop;
use async_winit::ThreadUnsafe;
use bytemuck::{cast_slice, Pod, Zeroable};
use fontdb::Family;
use futures_lite::prelude::*;
use glow::{HasContext, NativeShader};

// TODO:
// - modes
// - commands (requires modes)
// - file saving (requires commands)
// - paragraphs
// - dynamic character cache
// - put multiple characters per texture
// - figure out how to split words that are longer than a line

struct Window {
    window: async_winit::window::Window<ThreadUnsafe>,
    gl_context: glutin::context::PossiblyCurrentContext,
    _gl_display: glutin::display::Display,
    gl_surface: glutin::surface::Surface<glutin::surface::WindowSurface>,
}

impl Window {
    async fn new() -> (Self, glow::Context) {
        use glutin::config::ConfigTemplateBuilder;
        use glutin::context::{ContextAttributesBuilder, NotCurrentGlContext};
        use glutin::display::{DisplayApiPreference, GetGlDisplay, GlDisplay};
        use glutin::surface::GlSurface;
        use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};

        let window = async_winit::window::Window::<ThreadUnsafe>::new()
            .await
            .unwrap();

        let raw_handle = window.raw_display_handle();

        let display = unsafe {
            glutin::display::Display::new(raw_handle, DisplayApiPreference::Egl).unwrap()
        };

        let template = ConfigTemplateBuilder::new()
            .prefer_hardware_accelerated(Some(true))
            .with_depth_size(8)
            .with_stencil_size(0)
            .with_transparency(false)
            .build();
        let config = unsafe { display.find_configs(template).unwrap().next().unwrap() };
        let gl_display = config.display();

        let raw_window_handle = window.raw_window_handle();
        let context_attributes = ContextAttributesBuilder::new().build(Some(raw_window_handle));
        // by default, glutin will try to create a core opengl context. but, if it is not available, try to create a gl-es context using this fallback attributes
        let fallback_context_attributes = glutin::context::ContextAttributesBuilder::new()
            .with_context_api(glutin::context::ContextApi::Gles(None))
            .build(Some(raw_window_handle));
        let not_current_gl_context = unsafe {
            gl_display
                .create_context(&config, &context_attributes)
                .unwrap_or_else(|_| {
                    config
                        .display()
                        .create_context(&config, &fallback_context_attributes)
                        .expect("failed to create context even with fallback attributes")
                })
        };

        let (width, height): (u32, u32) = window.inner_size().await.into();
        let width = NonZeroU32::new(width).unwrap_or(NonZeroU32::MIN);
        let height = NonZeroU32::new(height).unwrap_or(NonZeroU32::MIN);
        let surface_attributes =
            glutin::surface::SurfaceAttributesBuilder::<glutin::surface::WindowSurface>::new()
                .build(window.raw_window_handle(), width, height);

        let gl_surface = unsafe {
            gl_display
                .create_window_surface(&config, &surface_attributes)
                .unwrap()
        };

        let gl_context = not_current_gl_context.make_current(&gl_surface).unwrap();

        gl_surface
            .set_swap_interval(
                &gl_context,
                glutin::surface::SwapInterval::Wait(NonZeroU32::MIN),
            )
            .unwrap();

        let gl = unsafe {
            use std::ffi::CString;
            glow::Context::from_loader_function(|s| {
                gl_display.get_proc_address(&CString::new(s).unwrap())
            })
        };

        (
            Window {
                window,
                gl_context,
                _gl_display: gl_display,
                gl_surface,
            },
            gl,
        )
    }

    fn resize(&self, size: PhysicalSize<u32>) {
        use glutin::surface::GlSurface;
        let width = match NonZeroU32::new(size.width) {
            Some(w) => w,
            None => return,
        };
        let height = match NonZeroU32::new(size.height) {
            Some(h) => h,
            None => return,
        };
        self.gl_surface.resize(&self.gl_context, width, height);
    }
    fn swap_buffers(&self) -> glutin::error::Result<()> {
        use glutin::surface::GlSurface;
        self.gl_surface.swap_buffers(&self.gl_context)
    }
}

fn main2(evl: EventLoop<ThreadUnsafe>) {
    let window_target = evl.window_target().clone();
    let font_db = {
        let mut db = fontdb::Database::new();
        db.load_system_fonts();
        assert!(!db.is_empty(), "Could not load system fonts!"); // TODO: fallback
        db
    };

    let font_face = {
        use fontdb::Weight;
        let weight = Weight::NORMAL;
        let families = &[Family::Serif, Family::SansSerif];
        let query = fontdb::Query {
            families,
            weight,
            stretch: fontdb::Stretch::Normal,
            style: fontdb::Style::Normal,
        };

        let font = font_db.query(&query).unwrap();
        font_db.face(font).unwrap()
    };

    let font_data = {
        // Can definitely be optimised to reduced allocations, but idc rn
        match &font_face.source {
            fontdb::Source::Binary(a) => a.as_ref().as_ref().to_vec(),
            fontdb::Source::File(f) => std::fs::read(f).unwrap(),
            fontdb::Source::SharedFile(_, a) => a.as_ref().as_ref().to_vec(),
        }
    };

    let font = {
        let settings = fontdue::FontSettings::default();
        fontdue::Font::from_bytes(&*font_data, settings).unwrap()
    };

    evl.block_on(async move {
        let text = RefCell::new(String::new());
        loop {
            // Wait for the application to be active.
            window_target.resumed().await;

            // Create a window.
            let (window, gl) = Window::new().await;
            let gl = Arc::new(gl);
            let renderer = Renderer::new(gl.clone(), &font);
            window.window.set_title("sed").await;

            let redraw = || async {
                let size = window.window.inner_size().await;
                renderer.draw(&text.borrow(), size);
                window.swap_buffers().unwrap();
            };

            let kb_ev = async {
                'input: loop {
                    let ev = window.window.keyboard_input().wait().await;

                    let input = ev.input;
                    if !matches!(input.state, ElementState::Pressed) {
                        continue 'input;
                    }

                    let kc = match input.virtual_keycode {
                        Some(kc) => kc,
                        None => continue 'input,
                    };

                    match kc {
                        VirtualKeyCode::Escape => return true,
                        _ => (),
                    };
                }
            };

            // Print the size of the window when it is resized.
            let print_size = async {
                window
                    .window
                    .resized()
                    .wait()
                    .for_each(|size| {
                        window.resize(size);
                        renderer.resize(size);
                        window.window.request_redraw();
                        println!("{:?}", size);
                    })
                    .await;

                true
            };

            let redraw_requested = async {
                loop {
                    window.window.redraw_requested().await;
                    redraw().await;
                }
            };

            // Wait until the window is closed.
            let close = async {
                window.window.close_requested().wait().await;
                println!("Close");
                true
            };

            // Wait until the application is suspended.
            let suspend = async {
                window_target.suspended().wait().await;
                false
            };

            let text_input = async {
                loop {
                    let c = window.window.received_character().await;
                    text.borrow_mut().push(c);
                    window.window.request_redraw();
                }
            };

            redraw().await;

            // Run all of these at once.
            let needs_exit = (redraw_requested)
                .or(kb_ev)
                .or(text_input)
                .or(print_size)
                .or(close)
                .or(suspend)
                .await;

            // If we need to exit, exit. Otherwise, loop again, destroying the window.
            if needs_exit {
                window_target.exit().await;
            } else {
                drop(window);
            }
        }
    });
}

fn main() {
    let evl = EventLoop::new();
    main2(evl);
}

use glutin::config::Config;
use glutin::prelude::*;

// Find the config with the maximum number of samples, so our triangle will be
// smooth.
pub fn gl_config_picker(configs: Box<dyn Iterator<Item = Config> + '_>) -> Config {
    configs
        .reduce(|accum, config| {
            let transparency_check = config.supports_transparency().unwrap_or(false)
                & !accum.supports_transparency().unwrap_or(false);

            if transparency_check || config.num_samples() > accum.num_samples() {
                config
            } else {
                accum
            }
        })
        .unwrap()
}

#[derive(Clone, Copy)]
struct Character {
    tex: glow::NativeTexture,
    metrics: fontdue::Metrics,
}

struct Renderer {
    program: glow::NativeProgram,
    vao: glow::NativeVertexArray,
    vbo: glow::NativeBuffer,
    ebo: glow::NativeBuffer,
    gl: Arc<glow::Context>,
    scale_loc: glow::NativeUniformLocation,
    mode_loc: glow::NativeUniformLocation,
    characters: HashMap<char, Character>,
}

const FONT_SIZE: f32 = 50.;

impl Renderer {
    pub fn new(gl: Arc<glow::Context>, font: &fontdue::Font) -> Self {
        let compile_shader = |src: &str, kind| -> NativeShader {
            unsafe {
                let shader = gl.create_shader(kind).unwrap();
                gl.shader_source(shader, src);
                gl.compile_shader(shader);
                if !gl.get_shader_compile_status(shader) {
                    let log = gl.get_shader_info_log(shader);
                    panic!("Could not compile shader: {log}")
                }
                shader
            }
        };

        unsafe {
            let vertex_shader =
                compile_shader(include_str!("shaders/vertex.glsl"), glow::VERTEX_SHADER);

            let fragment_shader = gl.create_shader(glow::FRAGMENT_SHADER).unwrap();
            gl.shader_source(fragment_shader, include_str!("shaders/fragment.glsl"));
            gl.compile_shader(fragment_shader);

            let program = gl.create_program().unwrap();

            gl.attach_shader(program, vertex_shader);
            gl.attach_shader(program, fragment_shader);

            gl.link_program(program);
            if !gl.get_program_link_status(program) {
                let log = gl.get_program_info_log(program);
                panic!("Failed to link: {log}");
            }
            let scale_loc = gl
                .get_uniform_location(program, "scale")
                .expect("Could not get scale uniform location");
            let mode_loc = gl
                .get_uniform_location(program, "mode")
                .expect("Could not get mode uniform location");

            gl.use_program(Some(program));

            gl.delete_shader(vertex_shader);
            gl.delete_shader(fragment_shader);

            let vao = gl.create_vertex_array().unwrap();
            gl.bind_vertex_array(Some(vao));

            let vbo = gl.create_buffer().unwrap();
            gl.bind_buffer(glow::ARRAY_BUFFER, Some(vbo));

            let ebo = gl.create_buffer().unwrap();
            gl.bind_buffer(glow::ELEMENT_ARRAY_BUFFER, Some(ebo));

            let pos_attrib = gl.get_attrib_location(program, "position").unwrap();
            let tex_coord_attrib = gl.get_attrib_location(program, "a_tex_coord").unwrap();

            let n = std::mem::size_of::<f32>() as i32;
            let stride = 4 * n;
            gl.vertex_attrib_pointer_f32(pos_attrib, 2, glow::FLOAT, false, stride, 0);
            gl.vertex_attrib_pointer_f32(tex_coord_attrib, 2, glow::FLOAT, false, stride, 2 * n);

            gl.enable_vertex_attrib_array(pos_attrib);
            gl.enable_vertex_attrib_array(tex_coord_attrib);

            gl.pixel_store_i32(glow::UNPACK_ALIGNMENT, 1);
            let mut characters = HashMap::new();
            for c in 0u8..128 {
                let c = c as char;

                let tex = gl.create_texture().unwrap();
                let (metrics, letter_data) = font.rasterize(c, FONT_SIZE);
                gl.bind_texture(glow::TEXTURE_2D, Some(tex));
                gl.tex_image_2d(
                    glow::TEXTURE_2D,
                    0,
                    glow::RED as i32,
                    metrics.width as i32,
                    metrics.height as i32,
                    0,
                    glow::RED,
                    glow::UNSIGNED_BYTE,
                    Some(&letter_data),
                );
                gl.tex_parameter_i32(
                    glow::TEXTURE_2D,
                    glow::TEXTURE_WRAP_S,
                    glow::CLAMP_TO_EDGE as _,
                );
                gl.tex_parameter_i32(
                    glow::TEXTURE_2D,
                    glow::TEXTURE_WRAP_T,
                    glow::CLAMP_TO_EDGE as _,
                );
                gl.tex_parameter_i32(
                    glow::TEXTURE_2D,
                    glow::TEXTURE_MIN_FILTER,
                    glow::LINEAR as _,
                );
                gl.tex_parameter_i32(
                    glow::TEXTURE_2D,
                    glow::TEXTURE_MAG_FILTER,
                    glow::LINEAR as _,
                );

                characters.insert(c, Character { tex, metrics });
            }
            gl.enable(glow::BLEND);
            gl.blend_func(glow::SRC_ALPHA, glow::ONE_MINUS_SRC_ALPHA);

            Self {
                program,
                vao,
                vbo,
                ebo,
                gl,
                scale_loc,
                mode_loc,
                characters,
            }
        }
    }

    pub fn draw(&self, text: &str, size: PhysicalSize<u32>) {
        let red = 0.1;
        let green = 0.1;
        let blue = 0.5;
        let alpha = 1.;
        let (indices, verts, rendered_chars) = {
            let mut tri_indices = Vec::new();
            let mut rendered_chars = Vec::with_capacity(text.len());
            let mut verts = Vec::new();
            let mut cursor = Vec2::new(0., FONT_SIZE + 10.);
            let mut words = text.split_inclusive(&[' ']).peekable();
            loop {
                let word = match words.peek() {
                    Some(word) => *word,
                    None => break,
                };
                let word_verts_start = verts.len();
                let word_idx_start = tri_indices.len();
                let word_chars_start = rendered_chars.len();
                let mut next_cursor = cursor;
                for c in word.chars() {
                    next_cursor.x += push_letter(
                        &mut verts,
                        &mut tri_indices,
                        &mut rendered_chars,
                        next_cursor,
                        &self.characters,
                        c,
                    );
                }

                if next_cursor.x > size.width as f32 {
                    cursor.y += FONT_SIZE + 10.;
                    cursor.x = 0.;
                    verts.truncate(word_verts_start);
                    tri_indices.truncate(word_idx_start);
                    rendered_chars.truncate(word_chars_start);
                    continue;
                }
                cursor = next_cursor;
                let _ = words.next();
            }
            push_quad(
                &mut verts,
                &mut tri_indices,
                cursor,
                Vec2::new(20., FONT_SIZE),
            );
            (tri_indices, verts, rendered_chars)
        };
        dbg!(indices.len());
        unsafe {
            self.gl.uniform_2_f32(
                Some(&self.scale_loc),
                2. / (size.width as f32),
                2. / (size.height as f32),
            );
            self.gl.use_program(Some(self.program));
            self.gl.active_texture(glow::TEXTURE0);
            self.gl.bind_vertex_array(Some(self.vao));

            self.gl.bind_buffer(glow::ARRAY_BUFFER, Some(self.vbo));
            self.gl
                .buffer_data_u8_slice(glow::ARRAY_BUFFER, cast_slice(&verts), glow::STATIC_DRAW);

            self.gl
                .bind_buffer(glow::ELEMENT_ARRAY_BUFFER, Some(self.ebo));
            self.gl.buffer_data_u8_slice(
                glow::ELEMENT_ARRAY_BUFFER,
                cast_slice(&indices),
                glow::STATIC_DRAW,
            );

            self.gl.clear_color(red, green, blue, alpha);
            self.gl.clear(glow::COLOR_BUFFER_BIT);

            self.gl.uniform_1_i32(Some(&self.mode_loc), 0);
            for (curr_idx, c) in rendered_chars.iter().enumerate() {
                let offset = (curr_idx * std::mem::size_of::<f32>()) as i32 * 6;
                self.gl.bind_texture(glow::TEXTURE_2D, Some(c.tex));
                self.gl
                    .draw_elements(glow::TRIANGLES, 6, glow::UNSIGNED_INT, offset)
            }
            self.gl.uniform_1_i32(Some(&self.mode_loc), 1);
            let cursor_quad_offset = (rendered_chars.len() * std::mem::size_of::<f32>()) as i32 * 6;
            self.gl
                .draw_elements(glow::TRIANGLES, 6, glow::UNSIGNED_INT, cursor_quad_offset)
        }
    }

    pub fn resize(&self, size: PhysicalSize<u32>) {
        let width = size.width as _;
        let height = size.height as _;
        unsafe {
            self.gl.viewport(0, 0, width, height);
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            self.gl.delete_program(self.program);
            self.gl.delete_buffer(self.vbo);
            self.gl.delete_buffer(self.ebo);
            self.gl.delete_vertex_array(self.vao);
        }
    }
}

fn push_quad(verts: &mut Vec<Vertex>, tri_indices: &mut Vec<Tri>, botleft: Vec2, size: Vec2) {
    verts.reserve(4);
    tri_indices.reserve(2);
    let top_l_idx = verts.len() as u32;
    verts.push(Vertex {
        coords: [botleft.x, botleft.y - size.y],
        tex_coords: [0., 0.],
    });
    let bot_l_idx = verts.len() as u32;
    verts.push(Vertex {
        coords: [botleft.x, botleft.y],
        tex_coords: [0., 1.],
    });
    let top_r_idx = verts.len() as u32;
    verts.push(Vertex {
        coords: [botleft.x + size.x, botleft.y - size.y],
        tex_coords: [1., 0.],
    });
    let bot_r_idx = verts.len() as u32;
    verts.push(Vertex {
        coords: [botleft.x + size.x, botleft.y],
        tex_coords: [1., 1.],
    });

    tri_indices.push([top_l_idx, bot_l_idx, bot_r_idx]);
    tri_indices.push([bot_r_idx, top_r_idx, top_l_idx]);
}

fn push_letter(
    verts: &mut Vec<Vertex>,
    tri_indices: &mut Vec<Tri>,
    chars: &mut Vec<Character>,
    cursor: Vec2,
    characters: &HashMap<char, Character>,
    c: char,
) -> f32 {
    let c = characters.get(&c).unwrap();
    chars.push(*c);

    let size = Vec2 {
        x: c.metrics.width as f32,
        y: c.metrics.height as f32,
    };
    let botleft = Vec2::new(
        cursor.x + c.metrics.bounds.xmin,
        cursor.y - c.metrics.bounds.ymin,
    );
    push_quad(verts, tri_indices, botleft, size);
    c.metrics.advance_width
}

type Point = [f32; 2];
type Tri = [u32; 3];
#[derive(Pod, Zeroable, Clone, Copy)]
#[repr(C)]
struct Vertex {
    coords: Point,
    tex_coords: Point,
}

mod math {
    #[derive(Clone, Copy)]
    pub(crate) struct Vec2 {
        pub x: f32,
        pub y: f32,
    }
    impl Vec2 {
        pub fn new(x: f32, y: f32) -> Self {
            Self { x, y }
        }
    }
}
use math::*;
