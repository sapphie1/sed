#version 330 core
out vec4 FragColor;
in vec2 tex_coord;

uniform sampler2D text;
uniform int mode;

void main() {
    vec4 c = vec4(1.0f, tex_coord.x, tex_coord.y, 1.0f);
    switch (mode) {
        case 0:
            vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, tex_coord).r);
            FragColor = sampled;
            break;
        case 1:
        default:
            FragColor = vec4(1.0);
    }
} 
