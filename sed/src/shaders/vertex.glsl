#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 a_tex_coord;
out vec2 tex_coord;

uniform vec2 scale;

void main() {
    vec2 screen_pos = scale * position;
    vec2 screen_coord = screen_pos - vec2(1., 1.);
    screen_coord.y *= -1;
    gl_Position = vec4(screen_coord, 0.0, 1.0);
    tex_coord = a_tex_coord;
}
